// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { app, data } = context;

    // Throw an error if we didn't get a make
    if(!data.make) {
      throw new Error('A car must have a make');
    }

    // Throw an error if we didn't get a model
    if(!data.model) {
      throw new Error('A car must have a model');
    }

    // The actual make
    const make = data.make
      // Make can't be longer than 30 characters
      .substring(0, 30);

    // The actual model
    const model = data.model
      // Model can't be longer than 30 characters
      .substring(0, 30);

    if ( data.year < 1885 || data.year > 2021 ) {
      throw new Error('Year must be between 1885 and 2021');
    }

    // Throw an error if we didn't get a plate
    if(!data.plate) {
      throw new Error('A car must have a plate');
    }

    const cars = await app.service('cars').find({
      query: {
        plate: data.plate
      }
    });

    // Throw an error if this is a duplicate plate
    if(cars.total > 0) {
      throw new Error('Duplicate plate');
    }

    // Get rid of any stray properties
    context.data = {
      make,
      model,
      year: data.year,
      plate: data.plate
    };

    return context;
  };
}
