import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Cars from './cars';
import Home from './home';

import client from './feathers';

class Application extends Component {
  
  constructor(props) {
    super(props);

    this.state = {};
  }
  
  componentDidMount() {
    const carService = client.service('cars');
    
    carService.find({
      query: {
        $limit: 25
      }
    }).then( carPage => {
      const cars = carPage.data;

      this.setState({ carService, cars });
    });

    // Remove a car from the list
    carService.on('removed', car => {
      ////////////////////////////////////////////////////
      // STEP 15
      // WRITE THE THE CODE TO HANDLE THE REMOVAL OF A CAR
      // START v
      ////////////////////////////////////////////////////
      console.log("removed id: " + car.id );
      const newcars = this.state.cars.filter((icar,index,arr) => {
        return icar.id !== car.id;
      });
      this.setState({ 
        cars: newcars
      });



      ////////////////////////////////////////////////////
      // END ^
      ////////////////////////////////////////////////////
    });
    

    // Add new car to the car list
    carService.on('created', car => this.setState({
      cars: this.state.cars.concat(car)
    }));

  }

  render() {
    return (
    <Router>
      <div>
        <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
          <h5 className="my-0 mr-md-auto font-weight-normal"><Link className="p-2 text-dark" to="/">CPSC 2650 Bootstrap Demo</Link></h5>
          <nav className="my-2 my-md-0 mr-md-3">
            <Link className="p-2 text-dark" to="/cars">Cars</Link>
          </nav>
        </div>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/cars">
            <Cars cars={this.state.cars} carService={this.state.carService} />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
  }
}

export default Application;
